﻿using UnityEngine;
using SpaceWalker.Camera;
using SpaceWalker.MapClasses;
using SpaceWalker.Player;
using SpaceWalker.MapClasses.MapDrawing;
using SpaceWalker.MapClasses.MapSearching;

namespace SpaceWalker
{
    public class MainController : MonoBehaviour
    {
        [SerializeField]
        private MainCameraController _mainCameraController;

        private MapSettings _mapSettings;
        private PlayerController _playerController;

        private MapDrawer _mapDrawer;
        private MapFiller _mapFiller;
        private MapSearcher _mapSearcher;

        private void Start()
        {
            _mapSettings = GetComponent<MapSettings>();
            _mapSettings.Init();
            _playerController = GetComponent<PlayerController>();
            _playerController.Init();
            _mapDrawer = GetComponent<MapDrawer>();
            _mapDrawer.Init();

            _mapFiller = new MapFiller(_mapSettings);
            _mapSearcher = new MapSearcher(_mapSettings, _mapFiller); ;
        }

        private void OnDestroy()
        {
            _mapFiller.Dispose();
            _mapSearcher.DisposeStrategies();
        }

        private void Update()
        {
            _playerController.UpdatePlayer();
            _mainCameraController.UpdateCameraWithPlayerPosition(_playerController.PlayerPosition);

            _mapFiller.CheckNearestFilledSectorsForPosition(_playerController.PlayerPosition);

            var visibleAreaCorners = _mainCameraController.VisibleAreaCorners;
            var playerData = _playerController.PlayerData;

            _mapSearcher.FindPlanetsInsideVisibleArea(visibleAreaCorners, playerData,
                _mainCameraController.IsSpecialModeActive);

            _mapDrawer.DrawMap(_mapSearcher, _playerController, _mainCameraController);
        }
    }
}

