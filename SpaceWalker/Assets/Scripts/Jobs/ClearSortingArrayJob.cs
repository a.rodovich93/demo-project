﻿using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using SpaceWalker.MapClasses;

namespace SpaceWalker.Jobs
{
    [BurstCompile]
    public struct ClearSortingArrayJob : IJobParallelFor
    {
        public NativeArray<long> PlanetSortingArray;

        public void Execute(int index)
        {
            PlanetSortingArray[index] = MapHelperFunctions.unsettedIdentifierValue;
        }
    }
}