﻿using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;

namespace SpaceWalker.Jobs
{
    [BurstCompile]
    public struct ClearSortingArrayCountersJob : IJobParallelFor
    {
        public NativeArray<int> PlanetSortingArrayCounters;

        public void Execute(int index)
        {
            PlanetSortingArrayCounters[index] = 0;
        }
    }
}

