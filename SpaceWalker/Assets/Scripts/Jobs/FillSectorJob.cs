﻿using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;

namespace SpaceWalker.Jobs
{
    [BurstCompile]
    public struct FillSectorJob : IJob
    {
        public NativeArray<int> MapSector;

        public Unity.Mathematics.Random RandomGenerator;

        public int SectorSideSize;

        public int RandomLower;
        public int RandomUpper;

        public void Execute()
        {
            if (!MapSector.IsCreated)
            {
                return;
            }

            for (int index = 0; index < SectorSideSize * SectorSideSize; index++)
            {
                var value = RandomGenerator.NextInt(RandomLower, RandomUpper);
                MapSector[index] = value;
            }
        }
    }
}