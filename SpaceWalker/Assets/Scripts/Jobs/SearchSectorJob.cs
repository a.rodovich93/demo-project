﻿using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;

namespace SpaceWalker.Jobs
{
    [BurstCompile]
    public struct SearchSectorJob : IJob
    {
        public NativeArray<int> MapSector;

        public NativeArray<long> PlanetSortingArray;
        public NativeArray<int> PlanetSortingArrayCounters;

        public int SectorSideSize;

        public int PlanetRatingMin;
        public int PlanetRatingRange;

        public int BestPlanetsMax;

        public int StartX;
        public int EndX;
        public int StartY;
        public int EndY;

        public long SectorIntegerIdentifier;

        public void Execute()
        {
            int index;

            for (int i = StartX; i < EndX; i++)
            {
                for (int j = StartY; j < EndY; j++)
                {
                    index = j * SectorSideSize + i;

                    int value = MapSector[index] - PlanetRatingMin;

                    if (value >= 0)
                    {
                        int counter = PlanetSortingArrayCounters[value];

                        int sortingPositionStart = value * BestPlanetsMax;

                        // Check that not all <bestPlanetsMax> planets with rating <value> already found
                        if (counter < BestPlanetsMax)
                        {
                            PlanetSortingArray[sortingPositionStart + counter] = index + SectorIntegerIdentifier;
                            counter++;
                            PlanetSortingArrayCounters[value] = counter;
                        }
                    }
                }
            }
        }
    }
}