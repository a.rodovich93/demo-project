﻿using UnityEngine;
using UnityEngine.UI;
using SpaceWalker.MapClasses;
using SpaceWalker.Structs;

namespace SpaceWalker.Camera
{
    public class MainCameraController : MonoBehaviour
    {
        public UnityEngine.Camera Camera;
        public Slider Slider;
        public MapSettings mapSettings;

        public bool IsSpecialModeActive { get; private set; }

        public RectCorners VisibleAreaCorners
        {
            get
            {
                Vector2Int bottomLeftIntegerPosition = new Vector2Int(Mathf.CeilToInt(_bottomLeft.x), Mathf.CeilToInt(_bottomLeft.y));
                Vector2Int topRightIntegerPosition = new Vector2Int(Mathf.FloorToInt(_topRight.x), Mathf.FloorToInt(_topRight.y));

                return new RectCorners
                {
                    BottomLeftIntegerPosition = bottomLeftIntegerPosition,
                    TopRightIntegerPosition = topRightIntegerPosition
                };
            }
        }

        private Vector3 _bottomLeft;
        private Vector3 _topRight;

        private float _currentZoom;
        private float _squareViewToSmallestScreenSideFactor = 1.0f;

        void CheckCurrentMode()
        {
            IsSpecialModeActive = (_currentZoom >= mapSettings.BasicModeMaxZoomSize / 2.0f);
        }

        void UpdateZoom(float yScale)
        {
            float zoom = Mathf.Clamp(
                Slider.value * (mapSettings.MaxZoomSize / 2.0f),
                (mapSettings.MinZoomSize / 2.0f), mapSettings.MaxZoomSize);

            _currentZoom = zoom;

            Camera.orthographicSize = zoom / (_squareViewToSmallestScreenSideFactor * yScale);
        }

        public void UpdateCameraWithPlayerPosition(Vector3 c_pos)
        {
            Camera.transform.position = new Vector3(c_pos.x, c_pos.y, -10);

            float viewOffsetX = 0.0f;
            float viewOffsetY = 0.0f;
            float viewScaleX = 1.0f;
            float viewScaleY = 1.0f;

            float screenWidth = Screen.width;
            float screenHeight = Screen.height;

            float aspectRatio = Camera.aspect;

            if (aspectRatio > 1.0f) // Width greater than height
            {
                viewOffsetX = ((aspectRatio - 1.0f) / 2.0f) / aspectRatio;
                viewScaleX = 1.0f / aspectRatio;
            }
            else
            {
                viewOffsetY = ((1.0f - aspectRatio) / 2.0f);
                viewScaleY = aspectRatio;
            }

            UpdateZoom(viewScaleY);

            CheckCurrentMode();

            _bottomLeft = Camera.ScreenToWorldPoint(new Vector3(
                screenWidth * Mathf.Lerp(0.5f, viewOffsetX, _squareViewToSmallestScreenSideFactor),
                screenHeight * Mathf.Lerp(0.5f, viewOffsetY, _squareViewToSmallestScreenSideFactor),
                0.0f));
            _topRight = Camera.ScreenToWorldPoint(new Vector3(
                screenWidth * Mathf.Lerp(0.5f, (viewOffsetX + viewScaleX), _squareViewToSmallestScreenSideFactor),
                screenHeight * Mathf.Lerp(0.5f, (viewOffsetY + viewScaleY), _squareViewToSmallestScreenSideFactor),
                0.0f));
        }
    }
}


