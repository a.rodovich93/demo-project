﻿using System.Collections.Generic;

namespace SpaceWalker.MapClasses
{
    public class DotsExtension : System.IDisposable
    {
        private List<System.IDisposable> _persistentNativeDisposables = new List<System.IDisposable>();

        protected T AllocatePersistentDisposable<T>(T c_t) where T : System.IDisposable
        {
            _persistentNativeDisposables.Add(c_t);
            return c_t;
        }

        public void Dispose()
        {
            foreach (System.IDisposable disposable in _persistentNativeDisposables)
            {
                disposable.Dispose();
            }
        }
    }
}