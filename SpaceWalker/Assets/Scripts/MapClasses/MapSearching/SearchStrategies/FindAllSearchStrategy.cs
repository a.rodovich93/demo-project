﻿using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using SpaceWalker.ObjectsInfo;
using SpaceWalker.Structs;

namespace SpaceWalker.MapClasses.MapSearching.SearchStrategies
{
    public class FindAllSearchStrategy : ISearchStrategy
    {
        private MapSettings _mapSettings;
        private List<PlanetInfo> _foundPlanets;

        public FindAllSearchStrategy(MapSettings mapSettings)
        {
            _mapSettings = mapSettings;
            _foundPlanets = new List<PlanetInfo>(mapSettings.VisiblePlanetsInBasicModeMaxCount);
        }

        public void Clear()
        {
            _foundPlanets.Clear();
        }

        public void FindPlanetsAtSector(NativeArray<int> mapSector, Vector2Int sectorIndex, LoopLimits2D sectorSearchLimits,
            PlayerInfo playerData, out bool searchCompleted)
        {
            int index;

            for (int i = sectorSearchLimits.StartX; i < sectorSearchLimits.EndX; i++)
            {
                for (int j = sectorSearchLimits.StartY; j < sectorSearchLimits.EndY; j++)
                {
                    index = j * _mapSettings.SectorSideSize + i;

                    int value = mapSector[index] - _mapSettings.PlanetRatingMin;
                    if (value >= 0)
                    {
                        _foundPlanets.Add(new PlanetInfo()
                        {
                            X = sectorIndex.x * _mapSettings.SectorSideSize + i,
                            Y = sectorIndex.y * _mapSettings.SectorSideSize + j,
                            Rating = mapSector[index]
                        });
                    }
                }
            }

            searchCompleted = false;
        }

        public List<PlanetInfo> GetFoundPlanets(PlayerInfo playerData)
        {
            return _foundPlanets;
        }
    }
}