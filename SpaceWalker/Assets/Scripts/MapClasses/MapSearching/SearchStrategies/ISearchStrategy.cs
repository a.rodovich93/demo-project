﻿using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using SpaceWalker.Structs;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.MapClasses.MapSearching.SearchStrategies
{
    public interface ISearchStrategy
    {
        void Clear();
        void FindPlanetsAtSector(NativeArray<int> mapSector, Vector2Int sectorIndex, LoopLimits2D sectorSearchLimits,
            PlayerInfo playerData, out bool searchCompleted);

        List<PlanetInfo> GetFoundPlanets(PlayerInfo playerData);
    }
}