﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using SpaceWalker.ObjectsInfo;
using SpaceWalker.Structs;
using SpaceWalker.Jobs;

namespace SpaceWalker.MapClasses.MapSearching.SearchStrategies
{
    public class FindBestNSearchStrategy : DotsExtension, ISearchStrategy
    {
        private NativeArray<long> _planetSortingArray;
        private NativeArray<int> _planetSortingArrayCounters;

        private MapSettings _mapSettings;
        private readonly int _planetSortingArrayLength;

        private const int _batchSize = 1000;

        public FindBestNSearchStrategy(MapSettings mapSettings)
        {
            _mapSettings = mapSettings;

            _planetSortingArrayLength = mapSettings.PlanetsRatingRange * mapSettings.BestPlanetsCount;
            AllocatePlanetSortingArrays();
        }

        public void Clear()
        {
            ClearPlanetSorters();
        }

        public List<PlanetInfo> GetFoundPlanets(PlayerInfo playerData)
        {
            int playerRating = playerData.Rating;
            int rowIndex = playerRating - _mapSettings.PlanetRatingMin;
            int sign;
            int counter = 0;
            bool searchCompleted = false;

            List<PlanetInfo> foundPlanets = new List<PlanetInfo>(_mapSettings.BestPlanetsCount);

            for (int i = 0; i < _mapSettings.PlanetsRatingRange * 2 - 1; i++)
            {
                sign = i % 2 == 0 ? -1 : 1;
                rowIndex += i * sign;

                if (rowIndex >= 0 && rowIndex < _mapSettings.PlanetsRatingRange)
                {
                    for (int j = 0; j < _planetSortingArrayCounters[rowIndex]; j++)
                    {
                        long val = _planetSortingArray[rowIndex * _mapSettings.BestPlanetsCount + j];

                        if (val != MapHelperFunctions.unsettedIdentifierValue)
                        {
                            Vector2Int pointCoord =
                                MapHelperFunctions.GetPositionForPointIntegerIdentifier(
                                    val,
                                    _mapSettings.SectorSideSize,
                                    _mapSettings.MaxSectorsAtOneDirection);
                            int c_rating = rowIndex;

                            var planetData = new PlanetInfo()
                            {
                                X = pointCoord.x,
                                Y = pointCoord.y,
                                Rating = c_rating
                            };

                            foundPlanets.Add(planetData);

                            counter++;

                            searchCompleted = (counter >= _mapSettings.BestPlanetsCount);

                            if (searchCompleted) break;
                        }
                        else break;
                    }

                    if (searchCompleted) break;
                }
            }
            return foundPlanets;
        }

        public void FindPlanetsAtSector(NativeArray<int> mapSector, Vector2Int sectorIndex,
            LoopLimits2D sectorSearchLimits, PlayerInfo playerData, out bool searchCompleted)
        {

            long sectorIntegerIdentifier = MapHelperFunctions.GetSectorIntegerIdentifier(sectorIndex,
                            _mapSettings.SectorSideSize,
                            _mapSettings.MaxSectorsAtOneDirection);

            SearchSectorJob searchSectorJob = new SearchSectorJob()
            {
                SectorSideSize = _mapSettings.SectorSideSize,
                BestPlanetsMax = _mapSettings.BestPlanetsCount,
                MapSector = mapSector,
                PlanetSortingArrayCounters = _planetSortingArrayCounters,
                PlanetRatingMin = _mapSettings.PlanetRatingMin,
                PlanetRatingRange = _mapSettings.PlanetsRatingRange,
                PlanetSortingArray = _planetSortingArray,
                SectorIntegerIdentifier = sectorIntegerIdentifier,

                StartX = sectorSearchLimits.StartX,
                EndX = sectorSearchLimits.EndX,

                StartY = sectorSearchLimits.StartY,
                EndY = sectorSearchLimits.EndY,
            };

            JobHandle handle = searchSectorJob.Schedule();
            handle.Complete();

            searchCompleted = IsNBestPlanetsWithRatingFound(playerData.Rating);
        }

        private bool IsNBestPlanetsWithRatingFound(int rating)
        {
            var index = rating - _mapSettings.PlanetRatingMin;
            return _planetSortingArrayCounters[index] >= _mapSettings.BestPlanetsCount;
        }

        private void AllocatePlanetSortingArrays()
        {
            _planetSortingArray = AllocatePersistentDisposable(
               new NativeArray<long>(_planetSortingArrayLength, Allocator.Persistent));
            _planetSortingArrayCounters = AllocatePersistentDisposable(
                    new NativeArray<int>(_mapSettings.PlanetsRatingRange, Allocator.Persistent));

            ClearPlanetSorters();
        }

        private void ClearPlanetSorters()
        {
            ClearPlanetSortingArray();
            ClearPlanetSortingArrayCounters();
        }

        private void ClearPlanetSortingArray()
        {
            ClearSortingArrayJob clearJob = new ClearSortingArrayJob()
            {
                PlanetSortingArray = _planetSortingArray,
            };

            JobHandle jobHandle = clearJob.Schedule(_planetSortingArrayLength, _batchSize);
            JobHandle.ScheduleBatchedJobs();
            jobHandle.Complete();
        }

        private void ClearPlanetSortingArrayCounters()
        {
            ClearSortingArrayCountersJob clearJob = new ClearSortingArrayCountersJob()
            {
                PlanetSortingArrayCounters = _planetSortingArrayCounters,
            };

            JobHandle jobHandle = clearJob.Schedule(_mapSettings.PlanetsRatingRange, _batchSize);
            JobHandle.ScheduleBatchedJobs();
            jobHandle.Complete();
        }
    }
}