﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using SpaceWalker.ObjectsInfo;
using SpaceWalker.MapClasses.MapSearching.SearchStrategies;
using SpaceWalker.Structs;

namespace SpaceWalker.MapClasses.MapSearching
{
    public class MapSearcher
    {
        public List<PlanetInfo> FoundPlanets { get; private set; }

        private readonly MapSettings _mapSettings;
        private readonly MapFiller _mapFiller;

        private ISearchStrategy _searchInSpecialModeStrategy;
        private ISearchStrategy _searchInBasicModeStrategy;

        public void DisposeStrategies()
        {
            ((DotsExtension)_searchInSpecialModeStrategy).Dispose();
        }

        public MapSearcher(MapSettings mapSettings, MapFiller mapFiller)
        {
            _mapSettings = mapSettings;
            _mapFiller = mapFiller;

            _searchInSpecialModeStrategy = new FindBestNSearchStrategy(mapSettings);
            _searchInBasicModeStrategy = new FindAllSearchStrategy(mapSettings);
        }

        public void FindPlanetsInsideVisibleArea(RectCorners visibleAreaCorners, PlayerInfo playerData,
            bool isSpecialModeActive)
        {
            LoopLimits2D visibleSectorsPositionLimits = MapHelperFunctions.GetSectorPositionLimitsInsideArea(visibleAreaCorners,
                _mapSettings.SectorSideSize);

            ISearchStrategy searchStrategy;

            if (isSpecialModeActive)
            {
                searchStrategy = _searchInSpecialModeStrategy;
            }
            else
            {
                searchStrategy = _searchInBasicModeStrategy;
            }

            searchStrategy.Clear();
            IterateVisibleSectorsWithSearchStrategy(visibleSectorsPositionLimits, visibleAreaCorners, searchStrategy, playerData);

            FoundPlanets = searchStrategy.GetFoundPlanets(playerData);
        }

        private void IterateVisibleSectorsWithSearchStrategy(LoopLimits2D visibleSectorsPositionLimits, RectCorners visibleAreaCorners,
            ISearchStrategy searchStrategy, PlayerInfo playerData)
        {
            for (int i = visibleSectorsPositionLimits.StartX; i <= visibleSectorsPositionLimits.EndX; i++)
            {
                for (int j = visibleSectorsPositionLimits.StartY; j <= visibleSectorsPositionLimits.EndY; j++)
                {
                    Vector2Int sectorIndex = new Vector2Int(i, j);

                    RectCorners sectorCorners = MapHelperFunctions.GetSectorCorners(sectorIndex, _mapSettings.SectorSideSize);

                    if (_mapFiller.FilledSectors.ContainsKey(sectorIndex))
                    {
                        NativeArray<int> mapSector = _mapFiller.FilledSectors[sectorIndex];

                        LoopLimits2D sectorSearchLimits = MapHelperFunctions.GetLoopLimitsForSearchInsideSector(visibleAreaCorners,
                            sectorCorners, _mapSettings.SectorSideSize);

                        bool canStopSearch;
                        searchStrategy.FindPlanetsAtSector(mapSector, sectorIndex, sectorSearchLimits,
                            playerData, out canStopSearch);

                        if (canStopSearch)
                        {
                            return;
                        }
                    }
                    else
                    {
                        Debug.Log($"NO SECTOR AT {sectorIndex.x} {sectorIndex.y}");
                    }
                }
            }
        }
    }
}