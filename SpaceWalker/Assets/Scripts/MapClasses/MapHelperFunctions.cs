﻿using UnityEngine;
using Unity.Mathematics;
using SpaceWalker.Structs;

namespace SpaceWalker.MapClasses
{
    public static class MapHelperFunctions
    {
        public const long unsettedIdentifierValue = long.MaxValue;

        public static Vector2Int GetSectorCoordsForWorldPositionAndSectorSize(
            Vector3 c_position,
            int sectorSize
            )
        {
            return new Vector2Int((int)Mathf.Floor(c_position.x * 1.0f / sectorSize),
                                   (int)Mathf.Floor(c_position.y * 1.0f / sectorSize));
        }

        // index always goes like index = j * size_x + i
        // where    i - goes for x
        //          j - goes for y
        // so to get i and j from index:
        // j = index / size_x
        // i = index % size_x
        public static long GetSectorIntegerIdentifier(
            Vector2Int sectorIndex,
            int sectorSize,
            int maxSectorsAtOneDirection)
        {
            long pointsAtOneSector = sectorSize * sectorSize;

            return sectorIndex.x * pointsAtOneSector
                    + sectorIndex.y * maxSectorsAtOneDirection * pointsAtOneSector;

        }

        public static void SplitPointIntegerIdentifierToParts(long pointIntegerIdentifier, int sectorSize,
            out int sectorPartIdentifier, out int pointPartIdentifier)
        {
            int pointsAtOneSector = sectorSize * sectorSize;

            sectorPartIdentifier = (int)(pointIntegerIdentifier / pointsAtOneSector);
            pointPartIdentifier = (int)(pointIntegerIdentifier % pointsAtOneSector);
        }

        public static Vector2Int GetCoordsForIntegerIdentifierPart(int integerIdentifierPart, int size_x)
        {
            return new Vector2Int(
                integerIdentifierPart % size_x,
                integerIdentifierPart / size_x);
        }

        public static Vector2Int GetPositionForPointIntegerIdentifier(
            long pointIntegerIdentifier,
            int fillSectorSize,
            int maxSectorsAtOneDirection) // turn to vector3
        {

            int sectorPartIdintifier;// = 0;
            int pointPartIdentifier;// = 0;

            SplitPointIntegerIdentifierToParts(pointIntegerIdentifier, fillSectorSize, out sectorPartIdintifier, out pointPartIdentifier);

            Vector2Int sectorCoord = GetCoordsForIntegerIdentifierPart(sectorPartIdintifier, maxSectorsAtOneDirection);
            Vector2Int pointCoord = GetCoordsForIntegerIdentifierPart(pointPartIdentifier, fillSectorSize);

            return new Vector2Int(
                sectorCoord.x * fillSectorSize + pointCoord.x,
                sectorCoord.y * fillSectorSize + pointCoord.y);
        }

        public static Vector2Int GetSectorBottomLeftPositionInWorldCoords(Vector2Int sectorIndex, int c_sectorSize)
        {
            return new Vector2Int(sectorIndex.x * c_sectorSize, sectorIndex.y * c_sectorSize);
        }

        public static Vector2Int GetSectorTopRightPositionInWorldCoords(Vector2Int sectorIndex, int c_sectorSize)
        {
            return new Vector2Int((sectorIndex.x + 1) * c_sectorSize - 1, (sectorIndex.y + 1) * c_sectorSize - 1);
        }

        public static LoopLimits2D GetSectorPositionLimitsInsideArea(RectCorners visibleAreaCorners, int sectorSideSize)
        {
            Vector2Int viewBottomLeftSectorCoord = MapHelperFunctions.GetSectorCoordsForWorldPositionAndSectorSize(
                    new Vector3(visibleAreaCorners.BottomLeftIntegerPosition.x, visibleAreaCorners.BottomLeftIntegerPosition.y),
                    sectorSideSize);

            Vector2Int viewTopRightSectorCoord = MapHelperFunctions.GetSectorCoordsForWorldPositionAndSectorSize(
                    new Vector3(visibleAreaCorners.TopRightIntegerPosition.x, visibleAreaCorners.TopRightIntegerPosition.y),
                    sectorSideSize);

            return new LoopLimits2D
            {
                StartX = viewBottomLeftSectorCoord.x,
                EndX = viewTopRightSectorCoord.x,
                StartY = viewBottomLeftSectorCoord.y,
                EndY = viewTopRightSectorCoord.y
            };
        }

        public static RectCorners GetSectorCorners(Vector2Int sectorIndex, int sectorSideSize)
        {
            Vector2Int sectorBottomLeftCornerPosition = GetSectorBottomLeftPositionInWorldCoords(
                sectorIndex, sectorSideSize);
            Vector2Int sectorTopRightCornerPosition = GetSectorTopRightPositionInWorldCoords(
                sectorIndex, sectorSideSize);

            return new RectCorners
            {
                BottomLeftIntegerPosition = sectorBottomLeftCornerPosition,
                TopRightIntegerPosition = sectorTopRightCornerPosition
            };
        }

        public static LoopLimits2D GetLoopLimitsForSearchInsideSector(RectCorners visibleAreaCorners, RectCorners sectorCorners, int sectorSideSize)
        {
            // if left bottom corner of view inside current sector
            // x_start = left bottom corner of VIEW position - left bottom corner of SECTOR position
            // otherwise zero, or greater than sectorSize, and that means, that this sector will be not processed

            // if right top corner of view inside current sector
            // x_end = sectorSize - (right top corner of SECTOR position - right top corner of VIEW position)
            // otherwise sectorSize, or less than zero

            // for y all is the same

            int bottomLeftDifferenceX = visibleAreaCorners.BottomLeftIntegerPosition.x - sectorCorners.BottomLeftIntegerPosition.x;
            int startX = math.max(bottomLeftDifferenceX, 0);

            int topRightDifferenceX = sectorCorners.TopRightIntegerPosition.x - visibleAreaCorners.TopRightIntegerPosition.x;
            int endX = math.min(sectorSideSize - topRightDifferenceX, sectorSideSize);

            int bottomLeftDifferenceY = visibleAreaCorners.BottomLeftIntegerPosition.y - sectorCorners.BottomLeftIntegerPosition.y;
            int startY = math.max(bottomLeftDifferenceY, 0);

            int topRightDifferenceY = sectorCorners.TopRightIntegerPosition.y - visibleAreaCorners.TopRightIntegerPosition.y;
            int endY = math.min(sectorSideSize - topRightDifferenceY, sectorSideSize);

            return new LoopLimits2D
            {
                StartX = startX,
                EndX = endX,

                StartY = startY,
                EndY = endY
            };
        }
    }
}