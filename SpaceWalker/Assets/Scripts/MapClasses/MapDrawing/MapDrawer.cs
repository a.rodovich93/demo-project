﻿using UnityEngine;
using SpaceWalker.Camera;
using SpaceWalker.MapClasses.MapDrawing.DrawMapStrategies;
using SpaceWalker.MapClasses.MapSearching;
using SpaceWalker.Player;

namespace SpaceWalker.MapClasses.MapDrawing
{
    public class MapDrawer : MonoBehaviour
    {
        private MapSettings _mapSettings;

        [SerializeField]
        private IMapDrawStrategy _basicModeDrawStrategy;

        [SerializeField]
        private IMapDrawStrategy _specialModeDrawStrategy;

        public void Init()
        {
            _mapSettings = GetComponent<MapSettings>();

            _basicModeDrawStrategy = GetComponent<DrawAllStrategy>();
            _basicModeDrawStrategy.Init(_mapSettings.VisiblePlanetsInBasicModeMaxCount);

            _specialModeDrawStrategy = GetComponent<DrawBestNStrategy>();
            _specialModeDrawStrategy.Init(_mapSettings.BestPlanetsCount);
        }

        public void DrawMap(MapSearcher mapSearcher, PlayerController playerController, MainCameraController mainCameraController)
        {
            IMapDrawStrategy mapDrawStrategy;
            ClearAllDrawStrategies();

            if (mainCameraController.IsSpecialModeActive)
            {
                mapDrawStrategy = _specialModeDrawStrategy;
            }
            else
            {
                mapDrawStrategy = _basicModeDrawStrategy;
            }

            mapDrawStrategy.DrawMapWithPlanetDataListAndCamera(mapSearcher.FoundPlanets, mainCameraController.Camera);
        }

        private void ClearAllDrawStrategies()
        {
            _specialModeDrawStrategy.Clear();
            _basicModeDrawStrategy.Clear();
        }
    }
}