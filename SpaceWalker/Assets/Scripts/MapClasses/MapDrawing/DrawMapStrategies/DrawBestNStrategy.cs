﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.MapClasses.MapDrawing.DrawMapStrategies
{
    public class DrawBestNStrategy : IMapDrawStrategy
    {
        [SerializeField]
        private RectTransform _canvas;

        [SerializeField]
        private Image _planetIconPrefab;
        private List<Image> _planetIcons;

        public override void Clear()
        {
            foreach (Image icon in _planetIcons)
            {
                icon.gameObject.SetActive(false);
            }
        }

        public override void DrawMapWithPlanetDataListAndCamera(List<PlanetInfo> planetDataList, UnityEngine.Camera camera)
        {
            for (int i = 0; i < planetDataList.Count; i++)
            {
                Image icon = _planetIcons[i];
                PlanetInfo planetData = planetDataList[i];

                DrawIconWithPlanetData(icon, planetData, camera);
            }
        }

        public override void Init(int maxIconsCount)
        {
            _planetIcons = new List<Image>();

            for (int i = 0; i < maxIconsCount; i++)
            {
                Image icon = Instantiate(_planetIconPrefab, Vector3.zero, Quaternion.identity, _canvas);
                _planetIcons.Add(icon);
                icon.gameObject.SetActive(false);

            }
        }

        private void DrawIconWithPlanetData(Image icon, PlanetInfo planetData, UnityEngine.Camera camera)
        {
            icon.gameObject.SetActive(true);

            Text planetText = icon.GetComponentInChildren<Text>();
            planetText.text = planetData.Rating.ToString();

            Vector3 iconPosition = camera.WorldToScreenPoint(new Vector3(planetData.X, planetData.Y, 0));
            icon.transform.position = iconPosition;
        }
    }
}