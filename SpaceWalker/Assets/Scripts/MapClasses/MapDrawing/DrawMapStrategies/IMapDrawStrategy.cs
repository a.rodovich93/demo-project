﻿using System.Collections.Generic;
using UnityEngine;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.MapClasses.MapDrawing.DrawMapStrategies
{
    public abstract class IMapDrawStrategy : MonoBehaviour
    {
        public abstract void Init(int maxIconsCount);
        public abstract void DrawMapWithPlanetDataListAndCamera(List<PlanetInfo> planetDataList, UnityEngine.Camera camera);
        public abstract void Clear();
    }
}