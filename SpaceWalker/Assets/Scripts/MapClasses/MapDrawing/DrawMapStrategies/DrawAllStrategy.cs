﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.MapClasses.MapDrawing.DrawMapStrategies
{
    public class DrawAllStrategy : IMapDrawStrategy
    {
        [SerializeField]
        private GameObject _planetIconPrefab;
        private List<GameObject> _planetIcons;

        override public void Init(int maxIconsCount)
        {
            _planetIcons = new List<GameObject>(maxIconsCount);

            for (int i = 0; i < maxIconsCount; i++)
            {
                GameObject c_icon = Instantiate(_planetIconPrefab, Vector3.zero, Quaternion.identity);
                _planetIcons.Add(c_icon);
                c_icon.gameObject.SetActive(false);
            }
        }

        override public void DrawMapWithPlanetDataListAndCamera(List<PlanetInfo> planetDataList, UnityEngine.Camera camera)
        {
            for (int i = 0; i < planetDataList.Count; i++)
            {

                GameObject icon = _planetIcons[i];
                PlanetInfo planetData = planetDataList[i];

                DrawIconInBasicModeWithPlanetData(icon, planetData);
            }
        }

        override public void Clear()
        {
            foreach (GameObject c_icon in _planetIcons)
            {
                c_icon.SetActive(false);
            }
        }

        private void DrawIconInBasicModeWithPlanetData(GameObject icon, PlanetInfo planetData)
        {
            icon.SetActive(true);

            TextMeshPro c_text = icon.GetComponentInChildren<TextMeshPro>();
            c_text.text = planetData.Rating.ToString();

            icon.transform.position = new Vector3(planetData.X, planetData.Y);
        }
    }
}