﻿using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Collections;
using SpaceWalker.Jobs;

namespace SpaceWalker.MapClasses
{
    public class MapFiller : DotsExtension
    {
        public Dictionary<Vector2Int, NativeArray<int>> FilledSectors { get; private set; }

        private MapSettings _mapSettings;
        private Unity.Mathematics.Random _randomSeedsGenerator;

        private NativeList<JobHandle> _fillSectorJobsHandleList;

        public MapFiller(MapSettings mapSettings)
        {
            _mapSettings = mapSettings;

            _randomSeedsGenerator = new Unity.Mathematics.Random((uint)System.DateTime.Now.Ticks);


            FilledSectors = new Dictionary<Vector2Int, NativeArray<int>>();
            _fillSectorJobsHandleList =
                AllocatePersistentDisposable(new NativeList<JobHandle>(Allocator.Persistent));
        }

        public void CheckNearestFilledSectorsForPosition(Vector3 c_pos)
        {
            Vector2Int center = MapHelperFunctions.GetSectorCoordsForWorldPositionAndSectorSize(c_pos, _mapSettings.SectorSideSize);

            int radius = _mapSettings.SectorsAtOneDirectionAtMaxZoom / 2;
            int offset = _mapSettings.SectorsAtOneDirectionAtMaxZoom % 2;

            int startX = center.x - radius;
            int endX = center.x + radius + offset;

            int startY = center.y - radius;
            int endY = center.y + radius + offset;

            _fillSectorJobsHandleList.Clear();

            for (int i = startX; i <= endX; i++)
            {
                for (int j = startY; j <= endY; j++)
                {
                    Vector2Int c_v = new Vector2Int(i, j);

                    bool c_sectorExists;
                    c_sectorExists = FilledSectors.ContainsKey(c_v);

                    if (!c_sectorExists)
                    {
                        JobHandle fillSectorJobHandle = FillSectorAtIndex(c_v);
                        _fillSectorJobsHandleList.Add(fillSectorJobHandle);
                    }
                }
            }

            JobHandle.CompleteAll(_fillSectorJobsHandleList);
        }

        private JobHandle FillSectorAtIndex(Vector2Int sectorIndex)
        {
            NativeArray<int> c_mapSector = AllocatePersistentDisposable(
                new NativeArray<int>(_mapSettings.SectorSideSize * _mapSettings.SectorSideSize, Allocator.Persistent));

            if (!c_mapSector.IsCreated)
            {
                Debug.Log("Memory Overflow");
            }

            FilledSectors.Add(sectorIndex, c_mapSector);

            FillSectorJob fillJob = new FillSectorJob()
            {
                MapSector = c_mapSector,
                RandomGenerator = new Unity.Mathematics.Random(_randomSeedsGenerator.NextUInt()),

                RandomLower = _mapSettings.PlanetRatingRandomMin,
                RandomUpper = _mapSettings.PlanetRatingRandomMax,
                SectorSideSize = _mapSettings.SectorSideSize,
            };

            JobHandle handle = fillJob.Schedule();
            return handle;
        }
    }
}