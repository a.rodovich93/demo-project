﻿using UnityEngine;

namespace SpaceWalker.MapClasses
{
    public class MapSettings : MonoBehaviour
    {
        public int BestPlanetsCount = 20;

        public float PlanetCreationProbability = 0.3f;
        public int PlanetRatingMax = 10000;
        public int PlanetRatingMin = 0;

        [HideInInspector]
        public int PlanetRatingRandomMin;
        [HideInInspector]
        public int PlanetRatingRandomMax;
        [HideInInspector]
        public int PlanetsRatingRange;

        [HideInInspector]
        public int SectorSideSize = 500;

        [HideInInspector]
        public int SectorsAtOneDirectionAtMaxZoom;

        public int MaxZoomSize = 10000;
        public int MinZoomSize = 5;
        public int BasicModeMaxZoomSize = 10;

        [HideInInspector]
        public int MaxSectorsAtOneDirection = 10000;

        [HideInInspector]
        public int VisiblePlanetsInBasicModeMaxCount;

        public Vector3 MapCenterPosition
        {
            get
            {
                var mapSideSize = SectorSideSize * MaxSectorsAtOneDirection;
                return new Vector3(mapSideSize / 2, mapSideSize / 2, 0);
            }
        }

        public void Init()
        {
            VisiblePlanetsInBasicModeMaxCount = BasicModeMaxZoomSize * BasicModeMaxZoomSize; // Turn all things like this to math.pow
            PlanetsRatingRange = (PlanetRatingMax - PlanetRatingMin) + 1;

            CalculateSectorSize();
            CalculateSectorsAtOneDirectionAtMaxZoom();
            CalculateRandomLimitsForPlanetFilling();
        }

        private void CalculateSectorSize()
        {
            SectorSideSize = (int)Mathf.Sqrt(PlanetsRatingRange * BestPlanetsCount / PlanetCreationProbability) / 2;
        }

        private void CalculateRandomLimitsForPlanetFilling()
        {
            // (10000 - 0 + 1) / 0.3 = x / 1.0;
            // x = (10000 - 0 + 1) / 0.3

            float fullRandomRange_float = (PlanetRatingMax + 1 - PlanetRatingMin) / PlanetCreationProbability;
            // +1 because Random(min, max) return value up to max - 1
            int fullRandomRange = (int)Mathf.Ceil(fullRandomRange_float);
            // Ceil, because probabilty must be greater or equal to 0.3
            // Less range, greater probabilty

            PlanetRatingRandomMin = PlanetRatingMax - fullRandomRange;
            PlanetRatingRandomMax = PlanetRatingMax + 1;
        }

        private void CalculateSectorsAtOneDirectionAtMaxZoom()
        {
            SectorsAtOneDirectionAtMaxZoom = MaxZoomSize / SectorSideSize;
            if (MaxZoomSize % SectorSideSize > 0) SectorsAtOneDirectionAtMaxZoom += 1;

            if (SectorsAtOneDirectionAtMaxZoom < 2) SectorsAtOneDirectionAtMaxZoom = 2;
        }
    }
}