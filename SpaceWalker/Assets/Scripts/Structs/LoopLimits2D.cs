﻿
namespace SpaceWalker.Structs
{
    public struct LoopLimits2D
    {
        public int StartX;
        public int EndX;

        public int StartY;
        public int EndY;
    }
}