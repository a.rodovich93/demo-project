﻿using UnityEngine;

namespace SpaceWalker.Structs
{
    public struct RectCorners
    {
        public Vector2Int BottomLeftIntegerPosition;
        public Vector2Int TopRightIntegerPosition;
    }
}