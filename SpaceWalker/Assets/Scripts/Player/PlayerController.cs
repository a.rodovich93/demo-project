﻿using UnityEngine;
using SpaceWalker.ObjectsInfo;
using SpaceWalker.Camera;
using SpaceWalker.MapClasses;

namespace SpaceWalker.Player
{
    public class PlayerController : MonoBehaviour
    {
        public MainCameraController MainCameraController;

        [HideInInspector]
        public Vector3 PlayerPosition { get; private set; }

        public PlayerInfo PlayerData { get; private set; }

        private MapSettings _mapSettings;

        [SerializeField]
        private PlayerRepresentation _playerRepresentationInBasicMode;

        [SerializeField]
        private PlayerRepresentation _playerRepresentationInSpecialMode;

        private Vector3 _movementDirection;

        public void UpdatePlayer()
        {
            UpdatePlayerMovementDirection();

            UpdatePlayerPosition(_movementDirection);

            UpdatePlayerData();
        }

        private void UpdatePlayerMovementDirection()
        {
            _movementDirection = GetMovementDirection();
        }

        public void Init()
        {
            _mapSettings = GetComponent<MapSettings>();

            PlayerPosition = _mapSettings.MapCenterPosition;

            CreatePlayerData();
        }

        private void CreatePlayerData()
        {
            PlayerData = new PlayerInfo()
            {
                X = (int)PlayerPosition.x,
                Y = (int)PlayerPosition.y,
                Rating = Random.Range(_mapSettings.PlanetRatingMin, _mapSettings.PlanetRatingMax)
            };
        }

        private void UpdatePlayerData()
        {
            PlayerData.X = (int)PlayerPosition.x;
            PlayerData.Y = (int)PlayerPosition.y;
        }

        private Vector3 GetMovementDirection()
        {
            float dx = Input.GetAxis("Horizontal");
            float dy = Input.GetAxis("Vertical");

            Vector3 dv = new Vector3(
                dx != 0.0f ? dx / Mathf.Abs(dx) : 0,
                dy != 0.0f ? dy / Mathf.Abs(dy) : 0,
                0);

            return dv;
        }

        private void UpdatePlayerPosition(Vector3 movementDirection)
        {
            PlayerPosition += movementDirection;

        }

        private void UpdatePlayerVisualRepresentationWithMovementDirection(Vector3 movementDirection, UnityEngine.Camera camera)
        {
            HideAllRepresentations();

            if (MainCameraController.IsSpecialModeActive)
            {
                _playerRepresentationInSpecialMode.Show(movementDirection, PlayerData, camera);
            }
            else
            {
                _playerRepresentationInBasicMode.Show(movementDirection, PlayerData, camera);
            }
        }

        private void HideAllRepresentations()
        {
            _playerRepresentationInSpecialMode.Hide();
            _playerRepresentationInBasicMode.Hide();
        }

        private void LateUpdate()
        {
            UpdatePlayerVisualRepresentationWithMovementDirection(_movementDirection, MainCameraController.Camera);
        }
    }
}