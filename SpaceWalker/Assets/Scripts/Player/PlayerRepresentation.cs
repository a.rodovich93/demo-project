﻿using UnityEngine;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.Player
{
    public abstract class PlayerRepresentation : MonoBehaviour
    {
        public abstract void Show(Vector3 movementDirection, PlayerInfo playerData, UnityEngine.Camera camera);
        public abstract void Hide();
    }
}
