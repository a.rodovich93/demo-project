﻿using UnityEngine;
using TMPro;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.Player
{
    // Basic Mode
    public class AsGameObjectPlayerRepresentation : PlayerRepresentation
    {
        [SerializeField]
        private GameObject _playerRepresentationPrefab;
        private GameObject _playerRepresentation;
        private TextMeshPro _playerText;

        private void Start()
        {
            SetupRepresentation();
        }

        public override void Hide()
        {
            _playerRepresentation.SetActive(false);
        }

        public override void Show(Vector3 movementDirection, PlayerInfo playerData, UnityEngine.Camera camera)
        {
            _playerRepresentation.SetActive(true);
            _playerRepresentation.transform.position = new Vector3(playerData.X, playerData.Y, 0);

            UpdatePlayerRotation(movementDirection);

            var ratingText = playerData.Rating.ToString();
            SetPlayerText(ratingText);
        }

        private void SetPlayerText(string text)
        {
            _playerText.text = text;
        }

        private void SetupRepresentation()
        {
            _playerRepresentation = Instantiate(_playerRepresentationPrefab);

            _playerText = _playerRepresentation.GetComponentInChildren<TextMeshPro>();
            SetPlayerText("0");
        }

        private void UpdatePlayerRotation(Vector3 movementDirection)
        {
            float angle = Mathf.Atan2(movementDirection.y, movementDirection.x) * Mathf.Rad2Deg;
            float angleOffset = -90.0f;

            if (movementDirection != Vector3.zero)
            {
                var spaceshipRotation = Quaternion.Euler(0, 0, angle + angleOffset);
                var textRotation = Quaternion.identity;

                _playerRepresentation.transform.rotation = spaceshipRotation;
                _playerText.transform.rotation = textRotation;
            }
        }
    }
}