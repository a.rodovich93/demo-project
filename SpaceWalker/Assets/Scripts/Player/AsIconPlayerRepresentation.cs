﻿using UnityEngine;
using UnityEngine.UI;
using SpaceWalker.ObjectsInfo;

namespace SpaceWalker.Player
{
    // Special Mode
    public class AsIconPlayerRepresentation : PlayerRepresentation
    {
        [SerializeField]
        private RectTransform Canvas;

        [SerializeField]
        private Image _playerRepresentationPrefab;
        private Image _playerRepresentation;
        private Text _playerText;

        private void Start()
        {
            SetupRepresentation();
        }

        public override void Hide()
        {
            _playerRepresentation.gameObject.SetActive(false);
        }

        public override void Show(Vector3 movementDirection, PlayerInfo playerData, UnityEngine.Camera camera)
        {
            _playerRepresentation.gameObject.SetActive(true);

            var playerPositionInWorldSpace = new Vector3(playerData.X, playerData.Y, 0);

            var playerPositionInScreenSpace = camera.WorldToScreenPoint(playerPositionInWorldSpace);
            _playerRepresentation.transform.position = playerPositionInScreenSpace;

            var ratingText = playerData.Rating.ToString();
            SetPlayerText(ratingText);
        }

        private void SetupRepresentation()
        {
            _playerRepresentation = Instantiate(_playerRepresentationPrefab, Canvas);

            _playerText = _playerRepresentation.GetComponentInChildren<Text>();
            SetPlayerText("0");
        }

        private void SetPlayerText(string text)
        {
            _playerText.text = text;
        }
    }
}