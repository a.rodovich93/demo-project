# Demo project

That was a test project named SpaceWalker I created, focusing on optimization techniques.

## Brief explanation and requirements:

- [ ] Main task: implement 2D game about spaceship in a space

## Requirements

- [ ] Space is a grid of square cells
- [ ] Ship moves by this cells using WASD
- [ ] Ship moves from one cell to another instantly
- [ ] Cell filled with planet or nothing (planets is not less than 30% of all cells)
- [ ] Space is infinite (while returning to the same place there must be the same planets every time)
- [ ] Every planet has a "rating" from 0 to 10000 that appeared as a number above corresponding planet
- [ ] Ship also has a rating from 0 to 10000, that assigned at a game start

- [ ] Minimal visible area is NxN cells, where N = 5; Zoom is available and magnifies area to N = 10000
- [ ] From N >= 10 SPECIAL MODE is enabled, when displayed only P = 20 planets with the ratings closest to ship's rating

- [ ] In SPECIAL MODE objects must displayed in such way, that they must be always visible, regardless from their real size
